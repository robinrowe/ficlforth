# FiclForth

Ficl is a lightweight, efficient implementation of Forth designed to be incorporated into other programs, including embedded systems. Ficl is a Forth extension with an OOP model that can wrap existing data structures. Ficl stands for "Forth Inspired Command Language". For details, see see "doc/index.html".

Created by John Sadler in 2001 and unchanged since 2010, Robin Rowe retrieved it, added a CMake build system and did code clean-up. I couldn't get the Windows-specific source files to work, so I used the unix.c file instead.

https://sourceforge.net/projects/ficl/
https://www.linkedin.com/in/johnsadler/
 
## License

Open source BSD.

## Platforms

* Win64 (tested in MSVC 2019)
* Win32 (tested in MSVC 2019)
* Linux (untested)
* Mac (untested)
* FreeBSD (untested)
* Embedded (untested)

## Build

Uses the typical cmake ../.. for Linux. Below is for Windows 64-bit. For 32-bit specify -A Win32. 

	mkdir build
	cd build
	mkdir x64
	cd x64
	cmake ../.. -A x64

## pForth

I also created a CMake build system for pForth.

https://github.com/robinrowe/pforth


Robin Rowe 
2020/5/2